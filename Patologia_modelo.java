public class Patologia_modelo implements Patologia_interfaz{

	//ATRIBUTOS
	private String nombre;
	private Double valor;
	
	//CONSTRUCTOR
	Patologia_modelo(String nombre, Double valor){
		this.nombre = nombre;
		this.valor = valor;
	}
	
	//METODOS
	public String getNombre() {
		return this.nombre;
	}
	
	public Double getValor() {
		return this.valor;
	}

}
