import java.util.Random;

public class Persona {

	//ATRIBUTOS
	private int id, edad, cont, dosis;
	private String estado;
	private Double formula;
	private boolean inoculado, portador;
	private Patologia_modelo afe, enf, vir;
	private Vacuna_modelo vac;
	Random rd = new Random();
		
	//CONSTRUCTOR
	Persona(int id, int edad){
		this.dosis = 0;
		this.id = id;
		this.edad = edad;
		this.cont = 0;
		this.estado = "Sano";
		this.inoculado = false;
		this.portador = false;
		
	}
		
	//METODOS
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public int getId() {
		return this.id;
	}
	
	public Boolean getPortador() {
		return this.portador;
	}
	
	public void setVirus(Patologia_modelo vir) {
		this.vir = vir;
		this.portador = true;
		this.estado = "Enfermo";
	}
	
	public void setAfeccion(Afeccion afe) {
		this.afe = afe;
	}
	
	public void setEnfermedad(Enfermedad enf) {
		this.enf = enf;
	}
	
	public Patologia_modelo getVirus() {
		return this.vir;
	}
	
	public Patologia_modelo getAfeccion() {
		return this.afe;
	}
	
	public Patologia_modelo getEnfermedad() {
		return this.enf;
	}
	
	public void setVacuna(Vacuna_modelo vac) {
		this.vac = vac;
		this.inoculado = true;
		this.dosis++;
	}
	
	public Boolean getInoculado() {
		return this.inoculado;
	}
	
	public Vacuna_modelo getVacuna() {
		return this.vac;
	}
	
	public void setFormula() {
		
		if(this.inoculado) {
			this.formula = ((this.edad * 0.0035) + getEnfermedad().getValor() + getAfeccion().getValor()) * 0.75;//getVacuna().getEfectividad();
		}
		
		else {
			this.formula = (this.edad * 0.0035) + getEnfermedad().getValor() + getAfeccion().getValor();
		}	
	}
	
	public void verificador() {	
		
		int temp1 = rd.nextInt(100)+1;
		if(this.formula*100 > 40) {
			this.estado = "Grave";
		}
		
		int temp2 = rd.nextInt(100)+1;
		if((this.formula*100) >= temp2) {
			this.estado = "Muerto";
		}
	}
	
	public  String getEstado() {
		return this.estado;
	}
	
	public Double getFormula() {
		return this.formula;
	}
	
	public int getDosis() {
		return this.dosis;
	}
	
	public void setContador() {
		this.cont++;
	}
	
	public int getContador() {
		return this.cont;
	}
	
	public int getEdad() {
		return this.edad;
	}
}
