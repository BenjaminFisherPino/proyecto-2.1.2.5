public class Vacuna_modelo implements Vacuna_interfaz{

	
	//ATRIBUTPS
	private Boolean estado;
	private String nombre;
	
	//CONSTRUCTOR
	Vacuna_modelo(String nombre){
		this.nombre = nombre;
		this.estado = false;
	}
	
	//METODOS
	
	public void setEstado() {
		this.estado = true;
	}
	
	public Boolean getEstado() {
		return this.estado;
	}
	
	public String getNombre() {
		return this.nombre;
	}
}
