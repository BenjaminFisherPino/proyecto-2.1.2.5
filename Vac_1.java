public class Vac_1 extends Vacuna_modelo{

	//ATRIBUTOS
	private Double efectividad;
	
	//CONSTRUCTOR
	Vac_1(String nombre){
		super(nombre);
		this.efectividad = 0.25;
	}
	
	//METODOS
	public Double getEfectividad() {
		return this.efectividad;
	}
}
