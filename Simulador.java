public class Simulador {

	//ATRIBUTOS
	private int pasos;
	
	//CONSTRUCTOR
	Simulador(int pasos, Comunidad com){
		com.contarInicio();
		for (int i = 0; i < pasos; i++) {
			com.cuentaPersonas();
			com.vacunar();
			com.infectar();
			com.recuperar();
			com.verificar();
			com.imprime();
		}
		com.contarFinal();
	}
}
