import java.util.Random;
import java.util.Collections;
import java.util.ArrayList;

public class Comunidad {

	//ATRIBUTOS
	private int habitantes, enf_iniciales, prom_contactos, cont1, cont2, cont3, promedio;
	private int cont_sanos, cont_enfermos, cont_graves, cont_muertos, cont_recuperados, cont_afecciones, cont_enfermedades;
	private Persona p;
	private Patologia_modelo a, e, vir;
	//private Vacuna_modelo vac_1,vac_2,vac_3;
	private Vac_1 vac_1;
	private Vac_2 vac_2;
	private Vac_3 vac_3;
	private ArrayList<Persona> poblacion, infectados, estrechos;
	//private ArrayList<Vacuna_modelo> lista_vac1, lista_vac2, lista_vac3;
	private ArrayList<Vac_1> lista_vac1;
	private ArrayList<Vac_2> lista_vac2;
	private ArrayList<Vac_3> lista_vac3;
	Random rd = new Random();
	
	//CONSTRUCTOR
	Comunidad(int habitantes, int enf_iniciales, int prom_contactos,Patologia_modelo vir){
		
		this.habitantes = habitantes;
		this.enf_iniciales = enf_iniciales;
		this.prom_contactos = prom_contactos;
		this.poblacion = new ArrayList<Persona>();
		//this.infectados = new ArrayList<Persona>();
		this.estrechos = new ArrayList<Persona>();
		this.cont1 = 0;
		this.cont2 = 0;
		this.cont3 = 0;
		this.cont_afecciones = 0;
		this.cont_enfermedades = 0;
		this.promedio = 0;
		this.cont_sanos = 0;
		this.cont_enfermos = 0;
		this.cont_graves = 0;
		this.cont_muertos = 0;
		this.cont_recuperados = 0;
		this.vir = vir;
				
		//ARRAYLISTS PARA LAS VACUNAS
		this.lista_vac1 = new ArrayList<Vac_1>();
		this.lista_vac2 = new ArrayList<Vac_2>();
		this.lista_vac3 = new ArrayList<Vac_3>();
		
		for(int i = 0; i < this.habitantes; i++) {
						
			int edad = rd.nextInt(100)+1;
			this.promedio = this.promedio + edad;
			p = new Persona(i+1,edad);
			this.poblacion.add(p);
		}
		
		Collections.shuffle(this.poblacion);
		
		//BUSCAR UN METODO PARA REDONDEAR EN CASO DE NUMEROS IMPARES
		for(int i = 0; i < 	this.habitantes;i++) {
			
			while(i < this.habitantes*0.25) {
				String enfermedades[] = new String[]{"Asma", "CerVas", "Fibrosis", "Hipertension", "Presion arterial"};
				String enfermedad = enfermedades[new Random().nextInt(enfermedades.length)];
				
				if(enfermedad.equals("Asma")) {
					this.poblacion.get(i).setEnfermedad(new Enfermedad("Asma",0.01));
				}
				
				else if (enfermedad.equals("Cervas")) {
					this.poblacion.get(i).setEnfermedad(new Enfermedad("Cervas",0.35));
				}
				
				else if (enfermedad.equals("Fibrosis")) {
					this.poblacion.get(i).setEnfermedad(new Enfermedad("Fibrosis",0.1));
				}
				
				else if (enfermedad.equals("Hipertension")) {
					this.poblacion.get(i).setEnfermedad(new Enfermedad("Hipertension",0.12));
				}
				
				else{
					this.poblacion.get(i).setEnfermedad(new Enfermedad("Presion arterial",0.19));
				}
				i++;
				this.cont_enfermedades++;
			}
		
		this.poblacion.get(i).setEnfermedad(new Enfermedad("Sin enfermedad",0.0));
		
		}
		
		Collections.shuffle(this.poblacion);
		
		for(int i = 0; i < habitantes; i++) {
			while(i < habitantes*0.65) {
				String afecciones[] = new String[]{"Obesidad","Desnutricion"};
				String afeccion = afecciones[new Random().nextInt(afecciones.length)];
				
				if(afeccion.equals("Obesidad")) {
					this.poblacion.get(i).setAfeccion(new Afeccion("Obesidad",0.25));
				}
				
				else {
					this.poblacion.get(i).setAfeccion(new Afeccion("Desnutricion",0.09));
				}
				i++;
				this.cont_afecciones++;
			}
			
			this.poblacion.get(i).setAfeccion(new Afeccion("Sin enfermedad",0.0));
		}
		
		Collections.shuffle(this.poblacion);	
		
		for(int i = 0; i < this.habitantes * 0.25; i++) {
			vac_1 = new Vac_1("Astrazeneca");
			this.lista_vac1.add(vac_1);
		}
		
		for(int i = 0; i < this.habitantes*0.16; i++) {
			vac_2 = new Vac_2("Pfizer");
			this.lista_vac2.add(vac_2);
		}
		
		for(int i = 0; i < this.habitantes*0.09; i++) {
			vac_3 = new Vac_3("Sinovac");
			this.lista_vac3.add(vac_3);
		}
				
		//SE INFECTAN A LAS N PRIMERAS PERSONAS DE LA LISTA
		for(int i = 0; i < this.enf_iniciales;i++) {
			this.poblacion.get(i).setVirus(vir);
		}
		
		Collections.shuffle(this.poblacion);
	}
	
	//METODOS
	public Patologia_modelo getVir() {
		return this.vir;
	}
	
	
	public void vacunar() {
		for (int i = 0; i < this.habitantes; i++) {
			int temp = rd.nextInt(3)+1;
			
			int cont_temp1 = 0;
			int cont_temp2 = 0;
			int cont_temp3 = 0;
			
			if((temp == 1) && (this.cont1 < 25) && (cont_temp1 < 5) && (!(this.poblacion.get(i).getInoculado()) || (this.poblacion.get(i).getDosis() < 2) && 
			(this.poblacion.get(i).getInoculado()) && this.poblacion.get(i).getVacuna().getNombre().contentEquals("Astrazeneca")) && !(this.poblacion.get(i).getEstado().equals("Recuperado") || this.poblacion.get(i).getEstado().equals("Muerto"))) {
				this.poblacion.get(i).setVacuna(this.lista_vac1.get(this.cont1));
				this.cont1++;
				cont_temp1++;
				if(this.poblacion.get(i).getDosis() == 2) {
					
				}
			}
			
			else if ((temp == 2) && (this.cont2 < 16) && (cont_temp2 < 4) && (!(this.poblacion.get(i).getInoculado()) || (this.poblacion.get(i).getDosis() < 2) && 
					(this.poblacion.get(i).getInoculado()) && this.poblacion.get(i).getVacuna().getNombre().contentEquals("Pfizer")) && !((this.poblacion.get(i).getEstado().equals("Recuperado") || (this.poblacion.get(i).getEstado().equals("Muerto"))))) {
				this.poblacion.get(i).setVacuna(this.lista_vac2.get(this.cont2));
				this.cont2++;
                cont_temp2++;
                if(this.poblacion.get(i).getDosis() == 2) {
                	
				}
			}
			
			else if ((temp == 3) && (this.cont3 < 9) && (cont_temp3 < 3) && !(this.poblacion.get(i).getInoculado() && (this.poblacion.get(i).getEstado().equals("Muerto"))) ){
				this.poblacion.get(i).setVacuna(this.lista_vac3.get(this.cont3));
				this.cont3++;
                cont_temp3++;
			}
		}
	}
	
	//INFECTAR
	public void infectar() {
		for(Persona p: this.poblacion) {
			if(p.getPortador()) {
				Collections.shuffle(this.poblacion);
				for(int i = 0; i < this.prom_contactos; i++) {
					this.estrechos.add(this.poblacion.get(i));
				}	
				comprueba_infeccion(this.estrechos);
				this.estrechos.clear();
			}
		}		
	}
	
	public void comprueba_infeccion(ArrayList<Persona> estrechos) {
		for (Persona r: estrechos){
            if(r.getEstado().equals("Sano")){
                //Se comprueba que exista un contacto fisico segun aleatoriedad
                if(this.rd.nextDouble() < this.prom_contactos){
                    //Se comprueba si realmente se pego la enfermedad
                    if(this.rd.nextDouble() < getVir().getValor()){                    	
                    	r.setVirus(this.vir);
                    	r.setEstado("Enfermo");
                    }
                }
            }
        }	
	}
	
	//RECUPERAR
	public void recuperar() {
		for(Persona p: this.poblacion) {
			if(p.getContador() == 5) {
				p.setEstado("Recuperado");
			}
		}
	}
	
	//VERIFICAR
	public void verificar() {
		for (Persona p: this.poblacion) {
			if(p.getPortador() && !(p.getEstado().equals("Muerto")) && !(p.getEstado().equals("Recuperado"))) {
				if(p.getInoculado()) {
					if(p.getVacuna().getNombre().equals("Astrazeneca")) {
						p.setFormula();
						p.verificador();
						p.setContador();
					}
					
					else if (p.getVacuna().getNombre().equals("Pfizer")) {
						if(p.getEstado().equals("Grave")) {
							p.setEstado("Enfermo");
						}
						p.setContador();
					}
					
					else {
						p.setEstado("Recuperado");
					}
				}
				
				else {
					p.setFormula();
					p.verificador();
					p.setContador();
				}
			}
		}
	}
	
	public void contarInicio() {
        int total_vacunas = lista_vac1.size() + lista_vac2.size() + lista_vac3.size();
        System.out.println("El total de poblacion es: " + this.habitantes + "\n" + 
                              "El total de personas con enfermedades: " + this.cont_enfermedades + "\n" +
                              "El total de personas con afecciones: " + this.cont_afecciones + "\n" +
                              "Promedio de edad de la comunidad: " + (this.promedio/this.habitantes) + "\n" +
                              "Vacunas disponibles: " + total_vacunas  + "\n");
    }
	
	public void contarFinal() {
		
		this.promedio = 0;
		for(Persona p: this.poblacion) {
			if(!p.getEstado().equals("Muerto")) {
				this.promedio = this.promedio + p.getEdad();
			}
		}
		
		System.out.println("El total de poblacion es: " + this.habitantes + "\n" +
                "El total de personas con enfermedades: " + this.cont_enfermedades + "\n" +
                "El total de personas con afecciones: " + this.cont_afecciones + "\n" +
                "El total de personas contagiadas: " + (this.habitantes - this.cont_sanos) + "\n" + 
                "El total de personas inoculadas: " + (this.cont1 + this.cont2 + this.cont3) + "\n" +
                "El total de personas recuperadas es de: " + this.cont_recuperados + "\n" +
                "El total de personas fallecidas es de: " + this.cont_muertos + "\n" + 
                "El promedio de edad de la comunidad viva final: " + (this.promedio/this.habitantes) + "\n");
	}
	
	

	//CONTAR_PERSONAS
	public void cuentaPersonas() {
		this.cont_sanos = 0;
		this.cont_enfermos = 0;
		this.cont_graves = 0;
		this.cont_muertos = 0;
		this.cont_recuperados = 0;
		for(Persona p: this.poblacion) {
			switch(p.getEstado()) {
			
			case("Sano"):
				this.cont_sanos++;
				break;
				
			case("Enfermo"):
				this.cont_enfermos++;
				break;
				
			case("Grave"):
				this.cont_graves++;
				break;
				
			case("Muerto"):
				this.cont_muertos++;
				break;
				
			case("Recuperado"):
				this.cont_recuperados++;
				
			}						
		}
	}
	
	public void imprime() {
		System.out.println("SANOS: " +  this.cont_sanos + "\nENFERMOS: " + this.cont_enfermos);
		System.out.println("GRAVES: " + this.cont_graves + "\nMUERTOS: " + this.cont_muertos);
		System.out.println("RECUPERADOS: " + this.cont_recuperados + "\n");
	}
}